package com.test.controllers;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files.Update;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.test.model.VersionFiles;
import com.test.model.VersionFilesImpl;

@Controller
public class DocumentController {
	
	 private static final String APPLICATION_NAME = "Google Drive API Java TestApp";
	 private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	 private static final java.io.File CREDENTIALS_FOLDER = new java.io.File(System.getProperty("user.home"), "credentials");
	 private static final String CLIENT_SECRET_FILE_NAME = "client_secret.json";
	 private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
	 private static NetHttpTransport HTTP_TRANSPORT;
	 private static Drive service;
	 
	 @Autowired
	 VersionFilesImpl versionService;
	 
		
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public ModelAndView viewHomePage(HttpServletRequest request, HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("home");
		
		try{
			googleDriveConnect();
		    HttpSession httpsession = request.getSession();
            String versionToOpen = request.getParameter("param1");
            String docToOpen = request.getParameter("param2");
            String fileId = request.getParameter("param3");
           	List<VersionFiles> versionHistory = versionService.getVersionHistory();
    		
    		//OPEN DOCUMENT
    		if(null!=versionToOpen){
    			String docContent = viewDocument(docToOpen,versionToOpen, fileId);
    			httpsession.setAttribute("docContent", docContent);
    			httpsession.setAttribute("versionToOpen", versionToOpen);
    			httpsession.setAttribute("docToOpen",docToOpen);
    		}
    		else{
    				httpsession.setAttribute("docContent", "");
    				httpsession.setAttribute("versionToOpen", "");
    		}
    		request.setAttribute("versionHistory",versionHistory);
    				
		}catch(Exception e)	{}
		//mav.addObject("home", new Document());
		return mav;
		
 }
	
	@RequestMapping(value="/home", method=RequestMethod.POST)
	public ModelAndView uploadDocument(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		ModelAndView mav = null;
		java.io.File uploadFile = null;
		VersionFiles versionfile = new VersionFiles();
		HttpSession httpsession = request.getSession();
		
		if(null!=request.getParameter("reset")){
			httpsession.setAttribute("docContent", "");
		}if(null!=request.getParameter("save")){
			
			String versionToOpen = request.getParameter("param1");
			String content = request.getParameter("mydoc");
			if(null!=content && !content.equals("")){
			String versionType = "Original";
			String docName = null;
			FileOutputStream out =null;
			docName = request.getParameter("docName");
			if(null == docName || docName.equals("")){
	    	  docName = (String) httpsession.getAttribute("docToOpen");
	    	  versionType = "Current";
			}
			String fileName = "D:\\Sanila\\CrackVerbal\\"+docName;
			XWPFDocument document = new XWPFDocument(); 
			out = new FileOutputStream(new java.io.File(fileName));
			XWPFParagraph paragraph = document.createParagraph();
			XWPFRun run = paragraph.createRun();
			run.setText(content);
			XWPFRun paragraphOneRunThree = paragraph.createRun();
			paragraphOneRunThree.setFontSize(30);
			document.write(out);
			uploadFile = new java.io.File(fileName);
			
			 File googleFile = createGoogleFile(null, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", docName, uploadFile);
			 Long fileVersion = googleFile.getVersion();
			 versionfile.setVersion(versionType);
			 versionfile.setFile_id(googleFile.getId());
			 versionfile.setDocument(docName);
			 versionfile.setCreatedDate(Calendar.getInstance().getTime());
			 versionService.saveVersionHistory(versionfile);
		}
		}
		List<VersionFiles> versionHistory = versionService.getVersionHistory();
		request.setAttribute("versionHistory",versionHistory);
		httpsession.setAttribute("docContent", "");
		httpsession.setAttribute("versionToOpen", "");
		mav = new ModelAndView("home");
	    return mav;
	}
	
	public void googleDriveConnect()throws Exception{

		// 1: Create CREDENTIALS_FOLDER
    	if (!CREDENTIALS_FOLDER.exists()) {
    		 CREDENTIALS_FOLDER.mkdirs();
        	}
    	     
    	HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    	
       //Read client_secret.json file & create Credential object.
        Credential credential = getCredentials(HTTP_TRANSPORT);
 
        //Create Google Drive Service.
        service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                .setApplicationName(APPLICATION_NAME).build();
        
        
 
   }

	public Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
	    java.io.File clientSecretFilePath = new java.io.File(CREDENTIALS_FOLDER, CLIENT_SECRET_FILE_NAME);
	    if (!clientSecretFilePath.exists()) {
	    	throw new FileNotFoundException("Please copy " + CLIENT_SECRET_FILE_NAME //
                + " to folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
	    }

	    // Load client secrets.
	    InputStream in = new FileInputStream(clientSecretFilePath);
	    GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

	    // Build flow and trigger user authorization request.
	    GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
            clientSecrets, SCOPES).setDataStoreFactory(new FileDataStoreFactory(CREDENTIALS_FOLDER))
                    .setAccessType("offline").build();

	    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}
	
	 // Create Google File from java.io.File
    public static File createGoogleFile(String googleFolderIdParent, String contentType, String customFileName, java.io.File uploadFile) throws IOException {
         AbstractInputStreamContent uploadStreamContent = new FileContent(contentType, uploadFile);
         return _createGoogleFile(googleFolderIdParent, contentType, customFileName, uploadStreamContent);
    }
    
    private static File _createGoogleFile(String googleFolderIdParent, String contentType, String customFileName, AbstractInputStreamContent uploadStreamContent) throws IOException {
    	File fileMetadata = new File();
        File file = new File();
        fileMetadata.setName(customFileName);
        List<String> parents = Arrays.asList(googleFolderIdParent);
        fileMetadata.setParents(parents);
        //String fileId = checkIfFileExists(customFileName);
        //System.out.println("FILE EXISTS = "+fileId);
        //if(null == fileId){
        	Drive.Files.Create ceateRequest = service.files().create(fileMetadata, uploadStreamContent).setFields("id, webContentLink, webViewLink, parents");
        	//ceateRequest.setKeepRevisionForever(Boolean.TRUE);
        	file = ceateRequest.execute();
        return file;
    }
    
    private static String checkIfFileExists (String docname) throws IOException{
    	String fileId = null;
    	FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
           for (File file : files) {
                //System.out.printf("%s (%s) (%s)\n", file.getName(), file.getId(), file.getVersion());
                if(file.getName().equals(docname)){
                	 	fileId = file.getId();
                }
            }  
        }
        return fileId;
    }
    
    
    private String viewDocument(String docname, String versionToOpen, String fileIdToOpen) {
		// TODO Auto-generated method stub
			
		String words = "";
		FileOutputStream openedFile;
		FileInputStream fis;
		java.io.File  f = null;
		if(null!=versionToOpen){
			try{
			FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
	        List<File> files = result.getFiles();
	        if (files == null || files.isEmpty()) {
	            System.out.println("No files found.");
	        } else {
	           for (File file : files) {
	                //System.out.printf("%s (%s) (%s)\n", file.getName(), file.getId(), file.getVersion());
	                if(file.getName().equals(docname) && file.getId().equals(fileIdToOpen)){
	                	String fileId = file.getId();
	                    String filename ="D:\\Sanila\\CrackVerbal\\FromRepo"+versionToOpen+".docx";
	        		    f = new java.io.File(filename); 
	        			openedFile =new FileOutputStream(f);
	                   // OutputStream outputStream = new ByteArrayOutputStream();
	                    service.files().get(fileId).executeMediaAndDownloadTo(openedFile);
	                    fis = new FileInputStream(filename);
	       			    XWPFDocument doc = new XWPFDocument(fis);
	       			    List<XWPFParagraph> paras = doc.getParagraphs(); //This list will hold the paragraphs
	       			    XWPFWordExtractor ex = new XWPFWordExtractor(doc);  //To get the words
	       			    for(XWPFParagraph p : paras){  //For each paragraph we retrieved from the document
	       			      words += p.getText();    //Add the text we retrieve to the words string  
	       			    }
	       		        XWPFDocument newDoc = new XWPFDocument(); 
	       			    XWPFParagraph para = newDoc.createParagraph();
	       			    XWPFRun run = para.createRun();     
	       			    run.setText(words);
	       			    newDoc.write(new FileOutputStream(new java.io.File("D:\\Sanila\\CrackVerbal\\temp.docx")));
	       			    f.delete();
	                    break;
	                }
	            }
	        }
			
		
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
			
	return words;

	}
}