package com.test.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.test.model.VersionFiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class VersionFilesImpl {
	
private JdbcTemplate jdbcTemplate;
	
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
public boolean saveVersionHistory(VersionFiles versionFiles) throws SQLException{
    	
    	boolean status = false;
    	boolean documentExists = false;
    	int rowsInserted = 0;
    	int document_id = 0;
    	String docname = versionFiles.getDocument();
    	String version_Type = versionFiles.getVersion();
    	String version_name =null;
    	String file_id  = versionFiles.getFile_id();
   		Timestamp created_on = new java.sql.Timestamp(versionFiles.getCreatedDate().getTime());
   		long created_date = created_on.getTime();
   		String created_by = "admin";
   		VersionFiles savedDocument = null;
   		VersionFiles savedVersion = null;
   		System.out.println("IN saveVersionHistory docname, created_date , version_name , file_id " + docname + created_date + version_name + file_id) ;
    	try{ 
          	    //check if document exists - START
    	    	String SQL_DOCUMENT_SELECT = "select document_id from document where name = '"+docname+"'" ;
    	    	List<VersionFiles> savedFiles = this.jdbcTemplate.query(SQL_DOCUMENT_SELECT, new BeanPropertyRowMapper(VersionFiles.class));
    	    	
    			if (savedFiles.size()>0){
    				documentExists = true;
    				savedDocument = savedFiles.get(0);
    			}
			    else 
			    	documentExists = false;
    			
    			String SQL_VERSION_SELECT = "select max(version_name) as version from version, document where version.document_id = document.document_id and name = '"+docname+"'" ;
    			List<VersionFiles> savedVersions = this.jdbcTemplate.query(SQL_VERSION_SELECT, new BeanPropertyRowMapper(VersionFiles.class));
    			if(savedVersions.size()>0){
    				savedVersion = savedVersions.get(0);
    			}
    			String version = savedVersion.getVersion();
    			String SQL_VERSION_INSERT = "INSERT INTO version (version_name, document_id, created_on, created_by, file_id) VALUES (?, ?, ?, ?, ?)";
    			if(documentExists){
    				document_id = savedDocument.getDocument_id();
    				double version_name_old = new Double(version);
    				double version_name_new = version_name_old + 1.0;
    				System.out.println("===============>AFTER version_name_new ");
    				version_name = String.valueOf(version_name_new);
    				Object[] versionParams = {version_name, document_id, created_on, created_by, file_id};
        	   		int[] versionTypes = {Types.VARCHAR, Types.INTEGER, Types.TIMESTAMP, Types.VARCHAR, Types.VARCHAR};
        	   		int versionRows = this.jdbcTemplate.update(SQL_VERSION_INSERT, versionParams, versionTypes);
        	   		status = true;
    			}else{
    				String SQL_DOCUMENT_INSERT = "INSERT INTO document (name, created_on, created_by) VALUES (?, ?, ?)";
    				version_name = "1.0"; 	    		
    	    		Object[] params = { docname, created_on, created_by};
        	   		int[] types = {Types.VARCHAR, Types.TIMESTAMP, Types.VARCHAR};
        	   		int rows = this.jdbcTemplate.update(SQL_DOCUMENT_INSERT, params, types);
        	   		        	   		
        	   		String SQL_DOCUMENT_INSERTED = "select document_id from document where name = '"+docname+"'" ;
        	    	List<VersionFiles> insertedFiles = this.jdbcTemplate.query(SQL_DOCUMENT_INSERTED, new BeanPropertyRowMapper(VersionFiles.class));
        	    	
        	    	if (insertedFiles.size()>0){
        	    		VersionFiles insertedDocument = insertedFiles.get(0);
        				document_id = insertedDocument.getDocument_id();
        				Object[] versionParams = {version_name, document_id, created_on, created_by, file_id};
            	   		int[] versionTypes = {Types.VARCHAR, Types.INTEGER, Types.TIMESTAMP, Types.VARCHAR, Types.VARCHAR};
            	   		int versionRows = this.jdbcTemplate.update(SQL_VERSION_INSERT, versionParams, versionTypes);
          	    	}	
        			status = true;
    			}
    	}catch(Exception e){}
    	return status;
	}

public List<VersionFiles> getVersionHistory(){
	List<VersionFiles> versionHistory = new ArrayList<VersionFiles>();
	try{
		
		String SQL_GET_DOCUMENT_VERSIONS = "select d.name, v.version_name, v.created_on, v.created_by, v.file_id from document d RIGHT JOIN version v on d.document_id =v.document_id ;";
		//versionHistory = this.jdbcTemplate.query(SQL_GET_DOCUMENT_VERSIONS, new BeanPropertyRowMapper(VersionFiles.class));
		versionHistory = this.jdbcTemplate.query(SQL_GET_DOCUMENT_VERSIONS,new RowMapper<VersionFiles>() {
		     public VersionFiles mapRow(ResultSet rs, int rowNum)
		    	       throws SQLException {
		    	 VersionFiles versionFiles = new VersionFiles();
		    	 versionFiles.setDocument(rs.getString("name"));
		    	 versionFiles.setVersion(rs.getString("version_name"));
		    	 versionFiles.setCreatedDate(rs.getDate("created_on"));
		    	 versionFiles.setCreatedBy(rs.getString("created_by"));
		    	 versionFiles.setFile_id(rs.getString("file_id"));
		     return versionFiles;
		     }
		});
	}catch(Exception e){}
		
	return versionHistory;
}

}
