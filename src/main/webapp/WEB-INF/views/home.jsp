<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GoogleDriveTestApp</title>
<script>
function validateForm() {
    var x = document.forms["myForm"]["mydoc"].value;
    if (x == "") {
        alert("Please enter Document content");
        return false;
    }
}
</script>
</head>
<body>
<div>
  <h2><center>GoogleDrive TestRepository </center></h2>
  <div style="margin-left:160px; margin-bottom:30px;">
        <table width ="660" border="1" cellpadding="5">
            <caption><h3>Version History</h3></caption>
           <tr>
                <th width = "25%">Name</th>
                <th width = "10%">Version</th>
                <th width = "45%">Created Date</th>
                <th width = "15%">Created By</th>
            </tr>
            <c:if test="${not empty versionHistory}">
            <c:forEach var="file" items="${versionHistory}">
                <tr>
                    <td width = "25%"><c:out value="${file.document}" /></td>
                    <td width = "20%"><a href="home?param1=${file.version}&param2=${file.document}&param3=${file.file_id}" >${file.version} </a> </td>
                    <td width = "35%"><c:out value="${file.createdDate}" /></td>
                    <td width = "15%"><c:out value="${file.createdBy}" /></td>
               </tr>
            </c:forEach>
            </c:if>
          </table>
    </div> 
  <div style="margin-left:160px">
     <form name="myForm" method="POST" action="home" onsubmit="return validateForm()">
     	<Label> Enter Document name <i> (ending with .docx)</i> :</Label><input type ="text" name="docName" id="docId"> 
     	<button style="margin-top:10px;margin-left:40px" type="submit" name="save"> UPLOAD </button> 
     	<button style="margin-top:10px;margin-left:40px" type="submit" name="reset" title="Clear contents"> RESET </button></br></br>
     	<c:if test="${not empty docContent}">
       <textarea style="margin-top:10px" name ="mydoc" id="docContent" rows="20" cols="80" required >${docContent}</textarea></br>
       </c:if>
       <c:if test="${empty docContent}">
       <textarea style="margin-top:10px" name ="mydoc" id="docContent" rows="20" cols="80"></textarea></br>
       </c:if>
       
      </form>
   </div>
   
  </div>
</body>
</html>